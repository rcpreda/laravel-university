<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThesisAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thesis_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('thesis_id')->unsigned();
            $table->string('original_name');
            $table->string('path');
            $table->string('type');
            $table->string('extension');
            $table->string('size');
            $table->timestamps();

            $table->foreign('thesis_id')->references('id')->on('theses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thesis_attachments');
    }
}
