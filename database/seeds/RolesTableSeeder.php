<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $roles = [
            'researcher',
            'moderator',
            'admin',
            'super-admin'
        ];


        foreach ($roles as $role) {
            Role::create(['name' => $role]);
        }

        //$role = Role::create(['name' => 'super-admin']);
        //$role->givePermissionTo(Permission::all());
    }
}
