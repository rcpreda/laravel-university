<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Permission;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = [
            'domain-list',
            'domain-create',
            'domain-edit',
            'domain-delete',
        ];

        $domains = [
            'Finance',
            'Finance Banks',
            'Finance Accountancy',
            'Machine Learning',
            'Public Management',
            'Artificial Intelligence',
        ];


        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

    }
}
