<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'SearchController@index')->name('search');
Route::get('/search/results', 'SearchController@list')->name('search.results');
Route::get('/view/theses/{slug}', 'SearchController@view')->name('search.view.theses');

Auth::routes();


//Route::get('/user/profile', 'DashboardController@profile')->name('user.profile');
//Route::resource('profile', 'ProfileController');

Route::middleware(['auth'])->group(function () {
    Route::get('/profile/edit', 'ProfileController@edit')->name('user.profile.edit');
    Route::put('/profile/update', 'ProfileController@update')->name('user.profile.update');
});

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('permissions','PermissionController');
    Route::resource('domains','DomainController');

    Route::get('notifications/domains','NotificationController@domains')->name('show.domain.notifications');
    Route::get('notifications/domains/{id}/modal','NotificationController@domains_show')->name('show.domain.notifications.modal');
    Route::post('notifications/{id}/verify','AjaxController@notification_verify')->name('ajax.notifications.verify');
    Route::post('notifications/{id}/update','AjaxController@notification_update')->name('ajax.notifications.update');
});

Route::middleware(['auth', 'profile'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/profile/avatar', 'ProfileController@avatar')->name('user.profile.avatar');
    Route::post('/profile/avatar/update', 'ProfileController@avatar_update')->name('user.profile.avatar.update');
    Route::get('/profile/password', 'ProfileController@change_password')->name('user.profile.change.password');
    Route::put('/profile/password/update', 'ProfileController@update_password')->name('user.profile.update.password');

    Route::resource('theses','ThesesController');
    Route::get('theses/{slug}', 'ThesesController@show')->name('theses.slug');
    Route::get('theses/download/{id}', 'ThesesController@download')->name('theses.download');
});

