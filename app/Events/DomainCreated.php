<?php

namespace App\Events;

use App\Domain;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;


class DomainCreated
{
    use Dispatchable, SerializesModels;

    /**
     * @var Domain $domain
     */
    public $domain;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }


}
