<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    /**
     * @var array
     */
    public static $values = [
        'infrastructure' => "Infrastructure",
        'software' => "Software",
        'hardware' => "Hardware",
    ];
}
