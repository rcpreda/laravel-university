<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserMetadata;
use Spatie\Permission\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;
use ScoutElastic\Searchable;

class User extends Authenticatable implements AuditableContract
{
    use Notifiable, HasRoles, Auditable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * @var array
     */
    public static $titles = [
        'Mr.' => "Mr.",
        'Mrs.' => "Mrs.",
        'Miss' => "Miss",
        'Ms.' => "Ms.",
        'Prof.' => "Prof.",
        'Rev.' => "Rev.",
        'Dr.' => "Dr.",
    ];

    protected $indexConfigurator = UsersIndexConfigurator::class;


    protected $mapping = [

        'properties' => [

            'id' => [
                'type' => 'integer'
            ],

            'full_name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase_normalizer'
            ],

        ]
    ];


    protected $searchRules = [
        //
    ];

    public function data()
    {
        return $this->hasOne(UserMetadata::class)->withDefault(function () {
            return new UserMetadata();
        });
    }

    /**
     * Theses.
     */
    public function theses()
    {
        return $this->hasMany(Thesis::class);
    }

    /**
     * Domains.
     */
    public function domains()
    {
        return $this->hasMany(Domain::class);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        //$array = $this->toArray();

        $array = [
            'id' => $this->id,
            //'first_name' => $this->first_name,
            //'last_name' => $this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name

        ];

        // Customize array...

        return $array;
    }

}
