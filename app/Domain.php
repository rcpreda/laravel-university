<?php

namespace App;

use App\Events\DomainCreated;
use App\Mail\DomainCreationAlert;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use ScoutElastic\Searchable;


class Domain extends Model implements AuditableContract
{

    use Notifiable, Auditable, Searchable;

    const UNDER_REVIEW = 2;
    const APPROVED = 1;
    const UNAPPROVED = 0;
    /**
     * @var array
     */
    public static $approval = [
        self::UNAPPROVED => "Unapproved",
        self::APPROVED => "Approved",
        self::UNDER_REVIEW => "Under review",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'approved',
        'user_id'
    ];

    protected $dispatchesEvents = [
        'created' => DomainCreated::class
    ];

    protected $indexConfigurator = DomainIndexConfigurator::class;

    protected $mapping = [

        'properties' => [

            'id' => [
                'type' => 'integer'
            ],

            'name' => [
                'type' => 'keyword',
                'normalizer' => 'lowercase_normalizer'
            ],

            'approved' => [
                'type' => 'integer'
            ],


        ]
    ];

    /*protected static function boot()
    {
        parent::boot();

        static::created( function($domain) {
            if ($domain->approved == self::UNDER_REVIEW) {
                $user = Auth::user();
                Mail::to($user)->send(new DomainCreationAlert($user, $domain));
            }
        });
    }*/

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }


    /**
     * Get the user that owns the phone.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Theses.
     */
    public function theses()
    {
        return $this->hasMany(Thesis::class);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('approved', 1);
    }

    /**
     * @param $query
     * @return array $manufacturersAsArray
     * @throws \Exception
     */
    public function scopeToOptionArray($query)
    {
        $types = $query->get(['id', 'name']);

        $typeAsArray = [];
        foreach ($types as $type) {
            $typeAsArray[$type->id] =  $type->name;
        }

        return $typeAsArray;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array =
            [
                'id' => $this->id,
                'name' => $this->name,
                'approved' => $this->approved,
            ];

        // Customize array...

        return $array;
    }
}
