<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

class UserMetadata extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 'user_metadata';

    /**
     * The attributes that are mass assignable.
     * Image crop size 600x500
     * @var array
     */
    protected $fillable = [
        'department',
        'research_area',
        'link',
        'avatar',
        'description',
    ];


    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
