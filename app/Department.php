<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * @var array
     */
    public static $values = [
        'education' => "Education",
        'agriculture' => "Agriculture",
        'science' => "Science",
        'military' => "Military",
        'it' => "Information Technologies",
    ];
}
