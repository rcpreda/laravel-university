<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;


use App\User;
use Illuminate\Support\Facades\Storage;

class Helper
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function avatar(int $userId)
    {
        $user = User::findOrFail($userId);
        $avatar = Storage::disk('public')->exists($user->data->avatar) ?
            Storage::disk('public')->url($user->data->avatar) : '/upload/testi_07.png';

        return $avatar;
    }

}