<?php

namespace App\Mail;

use App\Domain;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class DomainCreationAlert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Domain $domain
     */
    public $domain;

    /**
     * @var User $user;
     */
    public $user;


    /**
     * DomainCreationAlert constructor.
     * @param Domain $domain
     */
    public function __construct(User $user, Domain $domain)
    {
        $this->domain = $domain;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@vot.digital')
            ->markdown('emails.domain.alert');
    }
}
