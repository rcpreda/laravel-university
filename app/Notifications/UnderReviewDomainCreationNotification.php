<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UnderReviewDomainCreationNotification extends Notification
{
    use Queueable;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * UnderReviewDomainCreationNotification constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $message = sprintf("User: %s requested a new domain creation", $this->user->email);
        return [
            'message' => $message,
            'user_id' => $this->user->id,
            'user_first_name' => $this->user->first_name,
            'user_last_name' => $this->user->last_name,
            'user_email' => $this->user->email,
        ];
    }
}
