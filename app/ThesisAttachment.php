<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThesisAttachment extends Model
{

    protected $table = 'thesis_attachments';

    protected $fillable = [
        'thesis_id',
        'path',
        'original_name',
        'extension',
        'type',
        'size',
    ];

    /**
     * Get the post that owns the comment.
     */
    public function thesis()
    {
        return $this->belongsTo(Thesis::class);
    }


}
