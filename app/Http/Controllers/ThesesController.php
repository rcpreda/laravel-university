<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Events\DomainCreated;
use App\Http\Requests\ThesisUploadRequest;
use App\Mail\DomainCreationAlert;
use App\Notifications\UnderReviewDomainCreationNotification;
use App\Thesis;
use App\ThesisAttachment;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ThesesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $theses = Thesis::where(compact('user_id'))->
            whereIn('status', [1,2])->paginate(5);

        return view('theses.list')->with(compact('theses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $domains = Domain::active()->toOptionArray();
        return view('theses.create')->with(compact('domains'));
    }

    /**
     * @param ThesisUploadRequest $request
     */
    public function store(ThesisUploadRequest $request)
    {
        $user = $user = Auth::user();
        $all = $request->all();
        $files = $all['multiple'];
        $domain = null;

        if ($request->get('domain')) {
            $domain = Domain::find($request->get('domain'));
        }

        if ($request->get('domain_text') && !$domain) {

            try {

                $domain = Domain::create([
                    'name' => $request->get('domain_text'),
                    'approved' => Domain::UNDER_REVIEW,
                    'user_id' => $user->id
                ]);

            } catch (QueryException $e) {
                return redirect()->route('theses.index')
                    ->with('error','Try to use another domain name, the one you have chosen is already in use!');
            }

            //event(new DomainCreated($domain));

            //$domain->notify(new UnderReviewDomainCreationNotification($user));
        }

        $thesis = Thesis::create([
            'user_id' => $user->id,
            'domain_id' => $domain->id,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'status' => $domain->approved
        ]);


        if (count($files)) {
            /** @var UploadedFile $file */
            foreach ($files as $file) {
                $response = $file->store('files/'.$user->id . '/' . $thesis->id);
                $thesis->attachments()->create(
                    [
                        'path' => $response,
                        'size' => $file->getSize(),
                        'extension' => $file->getClientOriginalExtension(),
                        'type' => $file->getClientMimeType(),
                        'original_name' => $file->getClientOriginalName(),
                    ]
                );
            }

        }

        return redirect()->route('dashboard')
            ->with('success','Thesis created successfully');
    }

    /**
     * @param $slug
     * @return $this
     */
    public function show($slug)
    {
        $thesis = Thesis::where(compact('slug'))->firstOrFail();
        //abort_unless(auth()->user()->ownns($thesis), 403);
        return view('theses.show')->with(compact('thesis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($id)
    {
        $attachment = ThesisAttachment::findOrFail($id);
        $this->authorize('download', $attachment->thesis);
        $path = storage_path('app') . DIRECTORY_SEPARATOR . $attachment->path;
        return response()->download($path, $attachment->original_name);
    }
}
