<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Thesis;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function index()
    {
        return view('search.index');
    }

    /**
     * @param $slug
     * @return $this
     */
    public function view($slug)
    {
        $theses = Thesis::where(compact('slug'))->firstOrFail();
        //abort_unless(auth()->user()->ownns($thesis), 403);
        return view('search.view')->with(compact('theses'));
    }


    public function list(Request $request)
    {
        $searchTerm = $request->get('search');
        //print_r($searchTerm);

        $users = User::search($searchTerm)->get()->pluck('id')->toArray();
        $domains = Domain::search($searchTerm)->where('approved', 1)->get()->pluck('id')->toArray();
        $thesisRes = Thesis::search($searchTerm)->where('status', 1)->get()->pluck('id')->toArray();

        //dd($users);

        $thesisDomains = null;
        $thesisUsers = null;
        $thesisDB = null;

        //dd($thesisRes);
        $thesis = null;


        if ($users) {
            $thesisUsers = DB::table('theses')->whereIn('theses.user_id', $users)
                ->where('status', 1)
                ->leftJoin('users', 'users.id', '=', 'theses.user_id')
                ->leftJoin('domains', 'domains.id', '=', 'theses.domain_id')
                ->select(
                    'theses.*',
                    'users.first_name as uFirstName',
                    'users.last_name as uLastName',
                    'domains.name as domainName'
                );
            $thesis = $thesisUsers;
        }

        //dd($thesis->get());

        if ($domains) {
            $thesisDomains = DB::table('theses')->whereIn('theses.domain_id', $domains)
                ->leftJoin('users', 'users.id', '=', 'theses.user_id')
                ->leftJoin('domains', 'domains.id', '=', 'theses.domain_id')
                ->select(
                    'theses.*',
                    'users.first_name as uFirstName',
                    'users.last_name as uLastName',
                    'domains.name as domainName'
                );

            if ($thesis) {
                $thesis->union($thesisDomains);
            } else {
                $thesis = $thesisDomains;
            }
        }

        if ($thesisRes) {
            $thesisDB = DB::table('theses')->whereIn('theses.id', $thesisRes)
                ->where('status', 1)
                ->leftJoin('users', 'users.id', '=', 'theses.user_id')
                ->leftJoin('domains', 'domains.id', '=', 'theses.domain_id')
                ->select(
                    'theses.*',
                    'users.first_name as uFirstName',
                    'users.last_name as uLastName',
                    'domains.name as domainName'
                );

            if ($thesis) {
                $thesis->union($thesisDB);
            } else {
                $thesis = $thesisDB;
            }
        }

        if ($thesis) {
            $thesis = $thesis->paginate(2);
        }




        //dd($thesis);

        return view('search.list')->with(compact('thesis'));

    }

}
