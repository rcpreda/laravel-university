<?php

namespace App\Http\Controllers;

use App\Domain;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function domains()
    {

        /** @var User $user */
        //$user = Auth::user();

//        $domain = 'ss ff gggs';
//        $slug = str_slug($domain);
//
//        $validator = Validator::make(['slug' => $slug], [
//            'slug' => 'required|unique:domains,slug'
//        ]);
//
//        if ($validator->fails()) {
//            dd('here');
//            dd($validator->errors());
//        }
//
//        dd('nope');

        //$user->notify(new UnderReviewDomainCreationNotification($user));

        //$notification = DatabaseNotification::find('01e7a745-ce4c-4ad5-9df0-eb03d08d154e');
        //dd($notification->notifiable->owner);


        $notifications = DatabaseNotification::where('notifiable_type', Domain::class)
            ->whereNull('read_at')
            ->paginate(10);

//        foreach ($notifications as $notification) {
//           dd($notification->data['user_id']);
//        }

        //dd($notifications->count());

        //$domains = Domain::where('approved', 2)->get();
        //$countNotifications = 0;
        //foreach ($domains as $domain) {
        //    $countNotifications += $domain->unreadNotifications->count();
        //}

        //dd($countNotifications);

        return view('notifications.domain')->with(compact('notifications'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function domains_show($id)
    {
        $notification = DatabaseNotification::find($id);
        return view('notifications.domain-modal-view')->with(compact('notification'));
    }


}
