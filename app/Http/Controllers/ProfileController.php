<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePassword;
use App\Http\Requests\UpdateProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        return view('profile.edit')->with(compact('user'));
    }

    /**
     * @return $this
     */
    public function avatar()
    {
        $user = Auth::user();
        $avatar = Storage::disk('public')->exists($user->data->avatar) ?
                    Storage::disk('public')->url($user->data->avatar) : '/upload/testi_07.png';
        return view('profile.avatar')->with(compact('user'))->with(compact('avatar'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function avatar_update(Request $request)
    {
        if ($request->hasFile('avatar')) {
            $user = Auth::user();
            $avatar = $request->file('avatar');
            $filename = $user->id . '.' . $avatar->getClientOriginalExtension();
            $path = 'avatars';
            Storage::disk('public')->putFileAs($path, $avatar, $filename);
            $user->data->avatar = $path . '/' . $filename;
            $user->data->save();
        }

        return Redirect::route('user.profile.avatar');

    }

    /**
     * @param UpdateProfile $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfile $request)
    {
        //dd($request->all());
        $user = Auth::user();

        $user->title = $request->get('title');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->save();

        $user->data->department = $request->get('department');
        $user->data->research_area = $request->get('research');
        $user->data->description = $request->get('description');
        $user->data->link = $request->get('link');
        $user->data->save();


        flash('Your profile has been updated!')->success();
        return Redirect::route('dashboard');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        $user = Auth::user();
        return view('profile.change')->with(compact('user'));
    }

    /**
     * @param ChangePassword $request
     */
    public function update_password(ChangePassword $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();
        flash('Your password has been updated!')->success();
        return Redirect::route('dashboard');

    }
}
