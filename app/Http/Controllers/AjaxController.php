<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Thesis;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{

    public function notification_verify(Request $request, $id)
    {

        if ($request->ajax()) {
            /** @var DatabaseNotification $notification */
            $notification = DatabaseNotification::find($id);
            $domain = $notification->notifiable;
            $this->updateDomainStatus($domain);
            $notification->markAsRead();
            return response()->json(['success'=>'Domain ' . $domain->name . 'has been approved']);

        }

        return response()->json(['404'=>'Not allowed']);

    }

    public function notification_update(Request $request, $id)
    {

        if ($request->ajax()) {

            /** @var DatabaseNotification $notification */
            $notification = DatabaseNotification::find($id);
            $domain = $notification->notifiable;


            $domainName = $request->get('domain');
            $slug = str_slug($domainName);
            $validator = Validator::make(['slug' => $slug], [
                'slug' => 'required|unique:domains,slug'
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>' Domain name is taken: ' . $domainName]);
            }

            $this->updateDomainStatus($domain, $domainName);
            $notification->markAsRead();
            return response()->json(['success'=>'Domain ' . $domain->name . 'has been approved']);

        }

        return response()->json(['404'=>'Not allowed']);

    }

    /**
     * @param Domain $domain
     * @param null $domainName
     */
    protected function updateDomainStatus(Domain $domain, $domainName = null)
    {
        if ($domain instanceof Domain) {

            $theses = Thesis::where('domain_id', $domain->id)->get();
            foreach ($theses as $thesis) {
                $thesis->status = Thesis::APPROVED;
                $thesis->save();
            }

            $domain->approved = Domain::APPROVED;
            if ($domainName) {
                $domain->name = $domainName;
            }

            $domain->save();
        }
    }

}
