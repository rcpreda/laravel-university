<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user  = Auth::user();
        return [

            'title' => 'required|min:2|max:10',
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => [
                'required',
                'min:2',
                'max:255',
                 Rule::unique('users')->ignore($user->id)
            ],
            'link' => 'required|min:2|max:255|url',
            'department' => 'required',
            'research' => 'required',
            'description' => 'required|min:5|max:1024',
        ];
    }
}
