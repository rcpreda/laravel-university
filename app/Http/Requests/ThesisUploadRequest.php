<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ThesisUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function getValidatorInstance()
    {
        $this->formatSlug();
        $this->formatDomainSlug();

        return parent::getValidatorInstance();
    }

    protected function formatDomainSlug()
    {

        if ($this->request->has('domain_text')) {
            $this->merge([
                'slug_domain' => str_slug($this->request->get('domain_text'))
            ]);
        }
    }

    protected function formatSlug()
    {

        if ($this->request->has('title')) {
            $this->merge([
                'slug' => str_slug($this->request->get('title'))
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $request = $this->instance()->all();

        //dd($request);

        $rules = [
            'title' => 'required|min:2|max:255',
            'description' => 'required|min:5|max:1024',
            'slug' => 'required|unique:theses,slug'
        ];


        if (!empty($request['domain_text'])) {
            $rules['domain_text'] = 'required|min:3|max:60';
            $rules['slug_domain'] = 'required|unique:domains,slug';
        } else {
            $rules['domain'] = 'required|integer';
        }



        $files = $request['multiple'];
        $files_rules = 'required|mimes:docx,pdf|max:10240';
        if (count($files) > 0) {
            foreach ($files as $key => $file) {
                $rules['multiple.'.$key] = $files_rules;
            }
        }

        return $rules;
    }


    protected function createMessages()
    {
        $request = $this->instance()->all();
        $messages = [];
        $files = $request['multiple'];
        if (count($files) > 0) {
            foreach ($files as $key => $file) {
                $messages['multiple.'.$key.'.required'] = 'Select files!';
                $messages['multiple.'.$key.'.image'] = 'Only images!';
                $messages['multiple.'.$key.'.mimes'] = 'Only docx and pdf!';
                $messages['multiple.'.$key.'.size'] = 'File max 10M';
            }
        }

        $messages['slug.unique'] = 'Title has already been taken';
        $messages['slug_domain.unique'] = 'Domain name has already been taken';

        return $messages;
    }



    public function messages()
    {
        return $this->createMessages();
    }
}
