<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Profile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            $user = Auth::guard($guard)->user();
            /*if (!$user->data) {
                return Redirect::route('user.profile.edit');
            }*/

            if (!$user->data->department || !$user->data->research_area || !$user->data->description) {
                return Redirect::route('user.profile.edit');
            }

        }

        return $next($request);
    }
}
