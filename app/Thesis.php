<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class Thesis extends Model
{

    use Searchable;

    protected $table = 'theses';

    const UNDER_REVIEW = 2;
    const APPROVED = 1;
    const UNAPPROVED = 0;

    /**
     * @var array
     */
    public static $statuses = [
        self::UNAPPROVED => "Unapproved",
        self::APPROVED => "Approved",
        self::UNDER_REVIEW => "Under Review",
    ];


    protected $fillable = [
        'domain_id',
        'user_id',
        'title',
        'slug',
        'description',
        'status',
    ];


    protected $indexConfigurator = ThesisIndexConfigurator::class;

    protected $mapping = [

        'properties' => [

            'id' => [
                'type' => 'integer'
            ],

            'user_id' => [
                'type' => 'integer'
            ],

            'domain_id' => [
                'type' => 'integer'
            ],


            'title' => [
                'type' => 'text',
            ],

            'status' => [
                'type' => 'integer'
            ],
        ]
    ];


    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Get the user that owns the thesis.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the thesis domain.
     */
    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    /**
     * Attachments.
     */
    public function attachments()
    {
        return $this->hasMany(ThesisAttachment::class);
    }


    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array =
            [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'domain_id' => $this->domain_id,
                'title' => $this->title,
                'status' => $this->status,
            ];

        // Customize array...

        return $array;
    }



}
