<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RemoveRolesAndPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:remove-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all roles and permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('[' . Carbon::now() . '] Start command execution - remove all reles and permissions' );

        $this->comment('Remove data from permissions, model_has_permission and role has permission');

        DB::statement('truncate table role_has_permissions');
        DB::statement('truncate table model_has_permissions');
        DB::statement('truncate table model_has_roles');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $this->call('db:see', ['--class' => 'PermissionTableSeeder']);
        $this->call('db:see', ['--class' => 'RolesTableSeeder']);
        $this->call('db:see', ['--class' => 'DomainsTableSeeder']);

        $this->info('[' . Carbon::now() . '] End command execution - remove all reles and permissions' );
    }
}
