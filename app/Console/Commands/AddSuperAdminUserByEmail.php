<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class AddSuperAdminUserByEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:create-super-admin {email : Super admin email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create super admin user by email address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('[' . Carbon::now() . '] Start command execution - add super admin user' );

        $email = $this->argument('email');
        //dd($email);

        try {

            $user = User::where('email', '=', $email)->firstOrFail();
            $this->comment('Username found ' . $user->first_name);

            DB::table('model_has_roles')->where('model_id',$user->id)->delete();
            $user->assignRole(['super-admin']);

        } catch (ModelNotFoundException $e) {
            $this->comment($e->getMessage());
        }

        $this->info('[' . Carbon::now() . '] End command execution - add super admin user' );
    }
}
