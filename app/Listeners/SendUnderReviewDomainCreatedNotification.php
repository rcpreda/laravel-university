<?php

namespace App\Listeners;

use App\Domain;
use App\Events\DomainCreated;
use App\Mail\DomainCreationAlert;
use App\Notifications\UnderReviewDomainCreationNotification;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendUnderReviewDomainCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DomainCreated  $event
     * @return void
     */
    public function handle(DomainCreated $event)
    {
        if ($event->domain->approved == Domain::UNDER_REVIEW) {
            /** @var User $user */
            $user = Auth::user();
            Mail::to('contact@vot.digital')->send(new DomainCreationAlert($user, $event->domain));
            $event->domain->notify(new UnderReviewDomainCreationNotification($user));
        }
    }
}
