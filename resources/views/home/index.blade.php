@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.index')
@endsection

@section('search')
    @include('layouts.search.index')
@endsection

@section('content')
    <div class="section-title text-center clearfix">
        <h4>Featured Research Projects</h4>
        <hr>
        <p class="lead">Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel, sed quam nulla mauris iaculis. Erat eget vitae malesuada, tortor tincidunt porta lorem lectus.</p>
    </div>
@endsection