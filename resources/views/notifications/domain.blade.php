@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection

@section('content')
    <div class="row">

        <div class="content col-sm-12-8" style="padding-top: 50px;">
            <div class="post-padding">
                <div class="job-title nocover hidden-sm hidden-xs"><h5>Domain Notifications</h5></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="mytable" class="table table-bordred table-striped">

                                <thead>
                                <tr>
                                    <th>Domain</th>
                                    <th>Message</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>

                                @if(!$notifications->isEmpty())
                                    @foreach($notifications as $notification)
                                        <tr>
                                            <td><strong>{{ $notification->notifiable->name }}</strong></td>
                                            <td>{{ $notification->data['message'] }}</td>
                                            <td class="text-center">
                                                <p data-placement="top" data-toggle="tooltip" title="Approve">
                                                    <a href="#" role="button" data-toggle="modal" data-link="{{ route('show.domain.notifications.modal', $notification->id) }}" title="" class=" btn btn-success btn-xs my-modal-data">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">No Notification!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div><!-- end table -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end post-padding -->


            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div><!-- end col -->

    </div>

@endsection

@section('modals')
    @include('layouts.modal.domain-notifications')
@stop


@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('.my-modal-data').on('click', function(e){
                e.preventDefault();

                var dataURL = $(this).data('link');
                console.log(dataURL);

                $('.modal-body').load(dataURL,function(){
                    $('#domain-notifications').modal();
                });

            });
        });
    </script>
@stop