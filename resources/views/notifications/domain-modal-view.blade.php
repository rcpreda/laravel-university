    <style>
        .modal-content {
            margin: auto;
            padding: 15px;
            background-color: #f3f6f9;
        }
    </style>
    <div class="widget clearfix">
        <div class="post-padding item-price">
            <div class="row">
                <div class="col-md-12 toggle-dom-info" style="display: block">
                    <div class="content-title">
                        <h4>New domain registration</h4>
                    </div><!-- end widget-title -->
                    <p>
                        User: {{ $notification->notifiable->owner->first_name }} {{ $notification->notifiable->owner->last_name }}
                        wish to publish a thesis in {{ $notification->notifiable->name }}
                    </p>
                    <p>
                        Are you agree to publish this domain?
                    </p>

                    <a href="#" class="btn btn-custom btn-primary proceed-update">Yes</a>
                    <a href="#" class="btn btn-custom change-name">No</a>
                </div><!-- end col -->

                <div class="col-md-12 toggle-dom-form" style="display: none">

                    <div class="content-title">
                        <h4><i class="fa fa-lock"></i> Rename domain</h4>
                    </div><!-- end widget-title -->

                    <div class="alert alert-danger" role="alert" style="display: none">
                        &nbsp
                    </div>

                    <form id="submit-domain-data" class="submit-form">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <label>Domain:</label>
                                <input type="text" name="domain" class="form-control " value="{{ $notification->notifiable->name }}" placeholder="Domain">
                                <button class="btn btn-primary">Change</button>
                                <a href="#" class="btn btn-primary change-back">Back</a>
                            </div>
                        </div><!-- end row -->
                    </form>
                </div>

            </div><!-- end row -->
        </div><!-- end newsletter -->
    </div><!-- end post-padding -->

    <script type="text/javascript">
        $(document).ready(function() {

            $('.test-me-link').on('click', function(e){
                console.log('dddd');
            });


            $('.change-name').click(function() {
                $('.toggle-dom-info').toggle();
                $('.toggle-dom-form').toggle();
            });

            $('.change-back').click(function() {
                $('.toggle-dom-form').toggle();
                $('.toggle-dom-info').toggle();
            });





            $('.proceed-update').click(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                jQuery.ajax({
                    url: "{{ route('ajax.notifications.verify', $notification->id) }}",
                    method: 'post',
                    data: {
                        name: 'test',
                    },
                    success: function(result){
                        if (result.success) {
                            parent.location.reload();
                        }
                }});
            });


            $('#submit-domain-data').on('submit', function (e) {
                e.preventDefault();
                jQuery.ajax({
                    url: "{{ route('ajax.notifications.update', $notification->id) }}",
                    method: 'post',
                    data: $('form').serialize(),
                    success: function(result) {
                        $('.alert').hide();
                        if (result.success) {
                            parent.location.reload();
                        }

                        if (result.error) {
                            $('.alert').html(result.error);
                            $('.alert').show();
                        }
                }});
            });

        });
    </script>