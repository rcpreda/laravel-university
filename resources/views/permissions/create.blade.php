@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection

@section('content')
    <div class="content col-md-12" style="padding-top: 50px;">
        <div class="post-padding" style="padding-top: 30px;">
            <div class="job-title nocover hidden-sm hidden-xs"><h3>Create Permission</h3></div>


            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                    </div>
                </div>
            </div>


            @include('layouts.partials.errors.forms')



            {!! Form::open(['route' => 'permissions.store','method'=>'POST', 'class' => 'submit-form']) !!}
            <div class="row">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' =>'Permission name', 'required']) !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                        {!! Form::label('guard_name', 'Guard Name:', ['class' => 'control-label']) !!}
                        {!! Form::text('guard_name', old('guard_name'), ['class' => 'form-control', 'placeholder' =>'Guard name']) !!}
                        </div>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Create Permission</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection