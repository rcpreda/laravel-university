@extends('layouts.main')

@section('background', 'lb')



@section('content')
    <div class="row">

        <form class="submit-form customform" method="get" action="{{ route('search.results') }}">

            {!! Form::token(); !!}
            <div class="row">
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
                        <input type="text" class="form-control" placeholder="Search Keywords" aria-describedby="basic-addon2" name="search">
                    </div>
                </div><!-- end col -->

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <button class="btn btn-primary btn-block">Search Thesis</button>
                </div><!-- end col -->
            </div><!-- end row -->

            <!-- <div class="row listcheckbox">
                <div class="col-md-9">
                    <ul class="list-inline">
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_01" type="checkbox" class="styled">
                            <label for="checkbox_qu_01"><small>Freelancer</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_02" type="checkbox" class="styled">
                            <label for="checkbox_qu_02"><small>Part Time</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_03" type="checkbox" class="styled">
                            <label for="checkbox_qu_03"><small>Full Time</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_04" type="checkbox" class="styled">
                            <label for="checkbox_qu_04"><small>Temporary</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_05" type="checkbox" class="styled">
                            <label for="checkbox_qu_05"><small>Partnership</small>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 text-right">
                    <a href="#" class="readmore">View All</a>
                </div>
            </div><!-- end row -->
        </form>

        <div class="all-jobs job-listing clearfix">

            @if($thesis && $thesis->count())

                @foreach($thesis as $these)

                    <div class="job-tab">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="post-media">
                                    <a href="job-single.html"><img src="{!! Helper::avatar($these->user_id) !!}" alt="" class="img-responsive img-thumbnail"></a>
                                </div><!-- end media -->
                            </div><!-- end col -->

                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <!--<div class="badge freelancer-badge">Freelancer</div> -->
                                <h3><a href="{{ route('search.view.theses', $these->slug) }}" title="">{{ $these->title }}</a></h3>
                                <h5>{{ str_limit($these->description, 70, ' ...') }}</h5>
                                <small>
                                    <span>Publisher : <a href="#">{{ $these->uFirstName}} {{ $these->uLastName }}</a></span> <br>
                                    <span>Domain : <a href="#">{{ $these->domainName }}</a></span> <br>
                                    <span>Date : {{ $these->created_at }}</span>
                                </small>
                            </div><!-- end col -->
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="job-meta text-center" style="line-height: 120px;height: 120px;">
                                    <a href="{{ route('search.view.theses', $these->slug) }}" class="btn btn-primary btn-sm btn-block" style="display: inline-block; vertical-align: middle">View Theses</a>
                                </div>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end job-tab -->

                @endforeach

            @else
                <div class="job-tab">
                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3><a href="#" title="">No search results!</a></h3>
                        </div><!-- end col -->



                    </div><!-- end row -->
                </div><!-- end job-tab -->

            @endif

        </div><!-- end alljobs -->

        @if($thesis && $thesis->count())

        {!! $thesis->appends(request()->input())->links() !!}
        @endif
    </div>

@endsection

@section('modals')
    @include('layouts.modal.domain-notifications')
@stop


@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('.my-modal-data').on('click', function(e){
                e.preventDefault();

                var dataURL = $(this).data('link');
                console.log(dataURL);

                $('.modal-body').load(dataURL,function(){
                    $('#domain-notifications').modal();
                });

            });
        });
    </script>
@stop