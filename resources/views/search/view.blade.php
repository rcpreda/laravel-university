@extends('layouts.main')

@section('background', 'lb')



@section('content')
    <div class="row">

        <div class="content col-md-8">
            <div class="post-padding single-job single-shop">
                <div class="job-tab">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="post-media">
                                <img src="{!! Helper::avatar($theses->user_id) !!}" alt="" class="img-responsive">
                            </div><!-- end media -->
                        </div><!-- end col -->

                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="rating nomarginbot">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>

                            <h3>{{ $theses->title }}</h3>
                            <small>
                                <span>Publisher : <a href="#">{{ $theses->user->first_name }} {{ $theses->user->last_name }}</a></span>
                                <span>In : <a href="#">{{ $theses->domain->name }}</a></span> </small> <br>
                            <span>Date : {{ $theses->created_at }}</span>
                            </small>

                            <hr class="invis">


                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end job-tab -->
            </div><!-- end single-job -->

            <div class="post-padding  post-single-job">
                <div class="single-content">
                    <h4 class="small-title">Description</h4>
                    <p class="lead">

                        {{ $theses->description }}

                    </p>

                    <h5 class="small-title">Thesis attachments files</h5>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive job-table">
                                <table id="mytable" class="table table-bordred table-striped">

                                    <thead>
                                    <tr>
                                        <th>File Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(!$theses->attachments->isEmpty())
                                        @foreach($theses->attachments as $file)
                                            <tr>
                                                <td>
                                                    <h4><a href="#">{{ $file->original_name }}</a><br>
                                                    </h4>
                                                </td>
                                                <td>
                                                    <span data-plaxcement="top" data-toggle="tooltip" title="Download"><a class="btn btn-info btn-xs" href="{{ route('theses.download', $file->id) }}"><i class="fa fa-download"></i></a></span>
                                                </td>
                                            </tr>

                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3">No Files</td>
                                        </tr>
                                    @endif




                                    </tbody>
                                </table>
                            </div><!-- end table -->
                        </div>
                    </div><!-- end col -->


                    </div><!-- end row -->
                </div>
            </div><!-- end post-padding -->


        <div class="sidebar col-md-3">
            <div class="widget clearfix">
                <div class="widget-title">
                    <a href="{{ URL::previous() }}" class="btn btn-primary btn-custom">Back</a>
                </div><!-- end widget-title -->
            </div>
        </div>


    </div>
@endsection