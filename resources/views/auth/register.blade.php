@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')

    <div class="col-md-6 col-md-offset-3">
        <form class="submit-form customform loginform" method="POST" action="{{ route('register') }}">
            @csrf
            <h4>Create New Account</h4>

            @include('layouts.partials.errors.forms')

            <div class="form-group">
                <select class="selectpicker form-control" name="title" data-style="btn-default" style="height: 50px !important;">
                    <option value="Mr.">Mr.</option>
                    <option value="Mrs.">Mrs.</option>
                    <option value="Miss">Miss</option>
                    <option value="Ms.">Ms.</option>
                    <option value="Dr.">Dr.</option>
                    <option value="Prof.">Prof.</option>
                    <option value="Rev.">Rev.</option>
                </select>
            </div>


            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input id="first-name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name">
            </div>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                <input id="last-name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name">
            </div>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email address">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Re-Password">
            </div>
            <button class="btn btn-custom">{{ __('Register') }}</button>
            <div class="checkbox checkbox-primary">
                <input id="checkbox_qu_02" type="checkbox" class="styled">
                <label for="checkbox_qu_02"><small>Subscribe our newsletter</small></label>
            </div>
        </form>
    </div><!-- end col -->



@endsection
