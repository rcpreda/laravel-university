@extends('layouts.main')

@section('background', 'wb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')

    <div class="row">

        <div class="col-md-6 col-md-offset-3">
            <form class="submit-form customform loginform" method="POST" action="{{ route('login') }}">
                @csrf
                <h4>{{ __('Login') }}</h4>

                <a class="btn btn-link admin-credentials" href="#">
                    {{ __('Admin Credentials') }}
                </a>

                <a class="btn btn-link student-credentials" href="#">
                    {{ __('Student Credentials') }}
                </a>

                <div class="has-error input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="User name">

                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                </div>
                <button class="btn btn-custom"> {{ __('Login') }}</button>
                <div class="checkbox checkbox-primary">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="checkbox_qu_01"><small>{{ __('Remember Me') }}</small></label>
                </div>

                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif

            </form>
        </div><!-- end col -->

    </div><!-- end row -->

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.admin-credentials').click(function(e){
               e.stopPropagation();
               $('#email').val('admin@vot.digital');
               $('#password').val('admin123');
            });

            $('.student-credentials').click(function(e){
                e.stopPropagation();
                $('#email').val('student@vot.digital');
                $('#password').val('student123');
            });
        });
    </script>
@stop