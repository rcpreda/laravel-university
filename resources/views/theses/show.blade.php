@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')

    <div class="row">
        @include('layouts.sidemenu.left')

        <div class="content col-md-8" >
            <div class="post-padding" style="">
                <div class="row">
                    <div class="post-padding">
                        <div class="single-content">
                            <h4 class="small-title">{{ $thesis->title }}</h4>
                            <p class="lead"><strong>Status:</strong> {{ \App\Thesis::$statuses[$thesis->status] }}</p>

                            <p>
                                {{ $thesis->description }}
                            </p>

                            <h5 class="small-title">Thesis attachments files</h5>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive job-table">
                                        <table id="mytable" class="table table-bordred table-striped">

                                            <thead>
                                            <tr>
                                                <th>File Name</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(!$thesis->attachments->isEmpty())
                                            @foreach($thesis->attachments as $file)
                                                <tr>
                                                    <td>
                                                        <h4><a href="#">{{ $file->original_name }}</a><br>
                                                        </h4>
                                                    </td>
                                                    <td>
                                                        <span data-plaxcement="top" data-toggle="tooltip" title="Download"><a class="btn btn-info btn-xs" href="{{ route('theses.download', $file->id) }}"><i class="fa fa-download"></i></a></span>
                                                    </td>
                                                </tr>

                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">No Files</td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div><!-- end table -->
                                </div>
                            </div><!-- end col -->
                        </div>

                    </div><!-- end post-padding -->
                </div><!-- end row -->
            </div><!-- end post-padding -->


        </div><!-- end col -->
    </div><!-- end row -->

@stop