@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')
    <div class="row">
        @include('layouts.sidemenu.left')


        <div class="content col-md-8" style="padding-top: 50px;">
            <div class="post-padding" style="padding-top: 30px;">
                <div class="job-title nocover hidden-sm hidden-xs"><h5>Add thesis</h5></div>

                @include('layouts.partials.errors.forms')

                {!! Form::open(['method' => 'POST', 'route' => ['theses.store'],'enctype' => 'multipart/form-data', 'class' => 'submit-form']) !!}

                {!! Form::token(); !!}

                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <div class="form-group toggle domain-select" style="{{ old('domain_text') ? 'display: none' : '' }}">
                            {!! Form::label('domain', 'Domain:', ['class' => 'control-label']) !!}
                            {!! Form::select(
                                'domain',
                                $domains,
                                '',
                                ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'true', 'placeholder' => 'Choose Domain...'])
                            !!}
                        </div>

                        <div class="form-group toggle domain-input" style="{{ old('domain_text') ? 'display: visible' : 'display: none' }}">

                            {!! Form::label('domain_text', 'Add Domain:', ['class' => 'control-label']) !!}
                            {!! Form::text('domain_text', old('domain_text')? old('domain_text') : '' , ['class' => 'form-control', 'placeholder' =>'Domain']) !!}


                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="#" class="domain-toggle" style="{{ old('domain_text') ? 'display: none' : '' }}">Or, create a new domain</a>
                                <a href="#" class="domain-toggle-back" style="{{ old('domain_text') ? 'display: visible' : 'display: none' }}">Or, select domain</a>
                            </div>

                        </div>
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
                        {!! Form::text('title', old('title')? old('title') : '' , ['class' => 'form-control', 'placeholder' =>'Title', 'required']) !!}
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                        {!! Form::textarea('description', old('description')? old('description') : '' , ['class' => 'form-control', 'placeholder' =>'Your thesis description']) !!}

                    </div>
                </div><!-- end row -->

                <hr class="invis">


                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <label class="control-label"> Add files (.docx or .pdf)</label>
                        <div class="form-group input-group">
                            <input type="file" name="multiple[]" class="form-control" required>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-add" style="line-height: 26px">+
						        </button>
                            </span>
                        </div>
                    </div>
                </div>


                <hr>

                <button class="btn btn-primary">Create</button>

                {!! Form::close() !!}
            </div><!-- end post-padding -->


        </div><!-- end col -->
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
    $(document).ready(function() {

        $('.domain-toggle').click(function() {
            $(this).toggle();
            $('.domain-toggle-back').toggle();
            $('.domain-select').toggle();
            $('.domain-input').toggle();
        });


        $('.domain-toggle-back').click(function() {
            $(this).toggle();
            $('.domain-toggle').toggle();
            $('.domain-input').toggle();
            $('.domain-select').toggle();
            $('#domain_text').attr('value', '');
        });

    });


    (function ($) {
        $(function () {

            var addFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-default btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');
                $formGroupClone.insertAfter($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };

            var removeFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                $formGroup.remove();
            };

            var countFormGroup = function ($form) {
                return $form.find('.form-group').length;
            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);

        });
    })(jQuery);

    </script>
@stop
