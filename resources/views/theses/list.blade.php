@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')

    <div class="row">
        @include('layouts.sidemenu.left')

        <div class="content col-md-8" style="padding-top: 50px;">
            <div class="post-padding" style="padding-top: 30px;">
                <div class="job-title nocover hidden-sm hidden-xs"><h5>My Theses</h5></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive job-table">
                            <table id="mytable" class="table table-bordred table-striped">

                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Domain</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>


                                @if(!$theses->isEmpty())
                                @foreach($theses as $thesis)

                                    <tr>
                                        <td>
                                            <h4>
                                                <a href=" {{ route('theses.slug', $thesis->slug) }}" data-toggle="tooltip" title="View Thesis">{{ $thesis->title }}</a><br>
                                                <small>
                                                    {{ str_limit($thesis->description, 50, ' ...') }}
                                                </small>
                                            </h4>
                                        </td>
                                        <td>
                                            {{ $thesis->domain->name }}
                                        </td>
                                        <td>
                                            @if($thesis->status == 1)
                                                <span data-placement="top" data-toggle="tooltip" title="Approved"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></span>
                                            @elseif($thesis->status == 2)
                                                <span data-placement="top" data-toggle="tooltip" title="Pending"><button class="btn btn-warning btn-xs"><i class="fa fa-refresh"></i></button></span>
                                            @endif
                                        </td>
                                        <td>
                                            <span data-plaxcement="top" data-toggle="tooltip" title="Edit"><a class="btn btn-primary btn-xs" href="{{ route('dashboard') }}"><i class="fa fa-cogs"></i></a></span>
                                            <span data-placement="top" data-toggle="tooltip" title="Remove"><button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button></span>
                                        </td>
                                    </tr>

                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">No Theses</td>
                                    </tr>
                                    <td></td>
                                @endif
                                </tbody>
                            </table>
                        </div><!-- end table -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end post-padding -->


            {!! $theses->render() !!}

        </div><!-- end col -->
    </div><!-- end row -->

@stop