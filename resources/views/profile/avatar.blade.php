@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')
    <div class="row">
        @include('layouts.sidemenu.left')


        <div class="content col-md-8" style="padding-top: 50px;">
            <div class="post-padding" style="padding-top: 30px;">
                <div class="job-title nocover hidden-sm hidden-xs"><h5>Profile Avatar</h5></div>
                <form id="submit" class="submit-form" enctype="multipart/form-data" action="{{ route('user.profile.avatar.update') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label class="control-label"><small>Please upload a profile photo. (400x400)</small></label>
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6">
                                    <div class="post-media">
                                        <img src="{{ $avatar }}" alt="" class="profile-image img-circle img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6 ">
                                    <div class="vcenter" >
                                        <h4 >{{ $user->first_name }} {{ $user->last_name }}</h4>
                                        <input type="file" name="avatar" required>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <button class="btn btn-primary">Update Avatar</button>
                </form>
            </div><!-- end post-padding -->


        </div><!-- end col -->
    </div>

@endsection