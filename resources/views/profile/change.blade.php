@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')
    <div class="row">
        @include('layouts.sidemenu.left')
        <div class="content col-md-8" style="padding-top: 50px;">

            <div class="post-padding" style="padding-top: 30px;">

                <div class="job-title nocover hidden-sm hidden-xs"><h5>Change Password</h5></div>

                @include('layouts.partials.errors.forms')

                {!! Form::open([
                    'method' => 'PUT',
                    'route' => ['user.profile.update.password'],
                    'class' => 'submit-form'
                ]) !!}
                @csrf
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {!! Form::label('old', 'Old Password:', ['class' => 'control-label']) !!}
                        {!! Form::password('old', ['class' => 'form-control']) !!}
                        <br>


                        {!! Form::label('password', 'New Password:', ['class' => 'control-label']) !!}
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                        <br>

                        {!! Form::label('password_confirm', 'New Password:', ['class' => 'control-label']) !!}
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}

                        <br>
                        <button class="btn btn-primary">Update Password</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div><!-- end post-padding -->


        </div><!-- end col -->
    </div>

@endsection