@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')
    <div class="row">
        @include('layouts.sidemenu.left')

        <div class="content col-md-8" style="padding-top: 50px;">
            <div class="post-padding" style="padding-top: 30px;">
                <div class="job-title nocover hidden-sm hidden-xs"><h5>{{ $user->first_name }}'s Profile</h5></div>

                @include('layouts.partials.errors.forms')

                {!! Form::open(['method' => 'PUT', 'route' => ['user.profile.update'], 'class' => 'submit-form']) !!}

                {!! Form::token(); !!}
                    <div class="row">
                        <div class="col-md-6 col-sm-12">

                            <div class="form-group">
                                {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
                                {!! Form::select(
                                    'title',
                                    App\User::$titles,
                                    $user->title,
                                    ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'false', 'placeholder' => 'Select...'])
                                !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <label class="control-label">First name</label>
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') ? old('first_name'): $user->first_name }}">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label class="control-label">Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') ? old('last_name'): $user->last_name }}">
                        </div>
                    </div><!-- end row -->

                    <hr class="invis">

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label class="control-label">Email <small>Your contact email address</small></label>
                            <input type="text" class="form-control" name="email" value="{{ old('email') ? old('email'): $user->email }}" >

                        </div>
                    </div><!-- end row -->

                    <hr class="invis">

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            {!! Form::label('link', 'Link:', ['class' => 'control-label']) !!}
                            {!! Form::text('link', old('link')? old('link') : $user->data->link , ['class' => 'form-control', 'placeholder' =>'http://', 'required']) !!}
                        </div>
                    </div>

                    <hr class="invis">

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {!! Form::label('department', 'Department:', ['class' => 'control-label']) !!}
                                {!! Form::select(
                                    'department',
                                    App\Department::$values,
                                    $user->data->department,
                                    ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'false', 'placeholder' => 'Select...'])
                                !!}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                                {!! Form::label('research', 'Research Area:', ['class' => 'control-label']) !!}
                                {!! Form::select(
                                    'research',
                                    App\Research::$values,
                                    $user->data->research_area,
                                    ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'false', 'placeholder' => 'Select...'])
                                !!}
                        </div>
                    </div>

                    <hr class="invis">

                    <div class="row">
                        <div class="col-md-12 col-sm-12">

                            {!! Form::label('description', 'Short Description', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description')? old('description') : $user->data->description , ['class' => 'form-control', 'placeholder' =>'Your short description']) !!}

                        </div>
                    </div><!-- end row -->

                    <hr class="invis">

                    <hr>

                    <button class="btn btn-primary">Update Profile</button>

                {!! Form::close() !!}

            </div><!-- end post-padding -->


        </div><!-- end col -->
    </div>

@endsection