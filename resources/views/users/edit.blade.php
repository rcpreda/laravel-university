@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection

@section('content')
    <div class="content col-md-12" style="padding-top: 50px;">
        <div class="post-padding" style="padding-top: 30px;">
            <div class="job-title nocover hidden-sm hidden-xs"><h3>Edit User</h3></div>


            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                    </div>
                </div>
            </div>


            @include('layouts.partials.errors.forms')

            {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'class' => 'submit-form']) !!}
            <div class="row">
                <div class="row">
                    <div class="col-xs-6 col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
                            {!! Form::select(
                                'title',
                                App\User::$titles,
                                null,
                                ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'false', 'placeholder' => 'Select...'])
                            !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        {!! Form::label('first_name', 'First name:', ['class' => 'control-label']) !!}
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' =>'First Name', 'required']) !!}
                    </div>

                    <div class="col-md-6 col-sm-12">
                        {!! Form::label('last_name', 'Last name:', ['class' => 'control-label']) !!}
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' =>'Last Name', 'required']) !!}
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' =>'Email', 'required']) !!}
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                        {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
                        {!! Form::password('password', ['placeholder' => 'Password','class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirm Password:', ['class' => 'control-label']) !!}
                            {!! Form::password('password_confirmation', ['placeholder' => 'Password Confirmation','class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                @can('assign-roles')

                <hr class="invis">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            {!! Form::label('roles', 'Roles:', ['class' => 'control-label']) !!}
                            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'selectpicker', 'data-style' => 'btn-default','multiple')) !!}
                        </div>
                    </div>
                </div>

                @endcan

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection