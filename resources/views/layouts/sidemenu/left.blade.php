<div class="sidebar col-md-4">
    <div class="post-padding clearfix">
        <ul class="nav nav-pills nav-stacked">
            <li class="{{ request()->is('profile/edit') ? 'active' : '' }}">
                <a href="{{ route('user.profile.edit') }}">
                    <span class="glyphicon glyphicon-off"></span>  My Profile
                </a>
            </li>
            <li class="{{ request()->is('profile/avatar') ? 'active' : '' }}">
                <a href="{{ route('user.profile.avatar') }}">
                    <span class="glyphicon glyphicon-user"></span>  Profile Avatar
                </a>
            </li>
            <li class="{{ request()->is('theses') ? 'active' : '' }}">
                <a href="{{ route('theses.index') }}">
                    <span class="glyphicon glyphicon-edit"></span> My Theses
                </a>
            </li>

            <li class="{{ request()->is('theses/create') ? 'active' : '' }}">
                <a href="{{ route('theses.create') }}">
                    <span class="glyphicon glyphicon-magnet"></span>  Add Thesis
                </a>
            </li>
            <li><a href="freelancer-passed-jobs.html"><span class="glyphicon glyphicon-bell"></span>  Notifications</a></li>
            <li class="{{ request()->is('profile/password') ? 'active' : '' }}">
                <a href="{{ route('user.profile.change.password') }}"><span class="glyphicon glyphicon-refresh">
                    </span>  Change Password
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <span class="glyphicon glyphicon-lock"></span> {{ __('Logout') }}
                </a>
            </li>
        </ul>
    </div><!-- end widget -->
</div><!-- end col -->