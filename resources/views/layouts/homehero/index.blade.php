<div class="parallax section homehero">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="home-message">
                    <h1>University Project<span class="element"></span></h1>
                    <p>We're among one of the best Thesis online directory websites. </p>
                    <div class="svg-wrapper">
                        <div class="ttext">
                            <a class="btn btn-custom" href="#">View All Theses
                                <span class="fa fa-angle-right"></span>
                            </a>
                        </div>
                        <svg height="57" width="200" xmlns="http://www.w3.org/2000/svg">
                            <rect class="shape" height="57" width="200" />
                        </svg>
                    </div>
                </div><!-- end message -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->