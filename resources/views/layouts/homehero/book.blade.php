<div class="parallax section parallax-off" style="background-image:url('/upload/fbg.jpg');">
    <div class="container">
        <div class="page-title text-center">
            <div class="heading-holder">
                <h1>Login</h1>
            </div>
            <p class="lead">Hello there, this is your custom login page tagline message.</p>
        </div>
    </div><!-- end container -->
</div><!-- end section -->