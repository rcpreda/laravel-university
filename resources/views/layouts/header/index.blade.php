<header class="header">
    <div class="container-fluid">
        <nav class="navbar navbar-default yamm">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" title="" href="{{ route('home') }}"><img src="/images/logoUSIB2.png" alt="" class="img-responsive" width="30" height="38"></a>
                </div>
                <!-- end navbar header -->
                <!-- end navbar header -->

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        @if (Route::has('login'))
                            @auth
                                <li><a title="" href="{{ route('dashboard') }}">Home</a></li>
                                @can('menu-admin')
                                <li class="dropdown yamm-half hasmenu left">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu">

                                        <li><a href="{{ route('show.domain.notifications') }}">Notifications</a></li>

                                    @can('user-list')
                                        <li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>
                                        <li><a class="nav-link" href="{{ route('domains.index') }}">Manage Domains</a></li>
                                    @endcan
                                    @can('role-list')
                                        <li><a class="nav-link" href="{{ route('roles.index') }}">Manage Roles</a></li>
                                    @endcan
                                    @can('permission-list')
                                        <li><a class="nav-link" href="{{ route('permissions.index') }}">Manage Permissions</a></li>
                                    @endcan
                                    </ul>
                                </li>
                                @endcan


                            @else
                                <li><a title="" href="{{ route('home') }}">Home</a></li>
                            @endauth
                        @endif
                        <li><a title="" href="#">Support</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        @if (Route::has('login'))
                                @auth
                                <li class="dropdown yamm-half hasmenu left">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome {{ Auth::user()->first_name }} <span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('user.profile.edit') }}">Edit Profile</a></li>
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}</a></li>

                                    </ul>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @else
                                <li><a href="{{ route('login') }}" role="button" data-toggle="modal" title="">Sign in</a></li>
                                @if (Route::has('register'))
                                <li><a href="{{ route('register') }}" role="button" data-toggle="modal" title="">Register</a></li>
                                @endif
                            @endauth
                        @endif


                        <li><a class="btn btn-primary" title="" href="{{ route('theses.create') }}">Submit Thesis</a></li>
                    </ul>
                    <!-- end dropdown -->
                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
        </nav>
        <!-- end nav -->
    </div>
    <!-- end container -->
</header>
<!-- end header -->