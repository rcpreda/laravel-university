<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 center-xs">
                <p class="topbar-text">
                    <strong><i class="fa fa-phone"></i></strong> +44 711990847&nbsp;&nbsp;
                    <strong><i class="fa fa-envelope-o"></i></strong> <a href="mailto:test@test.com">test@test.com</a>
                </p>
            </div><!-- end col -->

            <div class="col-md-6 col-sm-12 center-xs text-right">
                <div class="social-topbar">
                    <ul class="list-inline social-small">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end topbar -->