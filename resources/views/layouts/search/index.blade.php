<div class="sectionnr nopadding wb">
    <div class="container">
        <form class="submit-form customform" method="get" action="{{ route('search.results') }}">
            <div class="row">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
                        <input type="text" class="form-control" placeholder="Search Thesis" aria-describedby="basic-addon2" name="search">
                    </div>
                </div><!-- end col -->

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <button class="btn btn-primary btn-block">Search</button>
                </div><!-- end col -->
            </div><!-- end row -->

            <div class="row listcheckbox">
                <div class="col-md-9">
                    <ul class="list-inline">
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_01" type="checkbox" class="styled">
                            <label for="checkbox_qu_01"><small>Education</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_02" type="checkbox" class="styled">
                            <label for="checkbox_qu_02"><small>Agriculture</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_03" type="checkbox" class="styled">
                            <label for="checkbox_qu_03"><small>Science</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_04" type="checkbox" class="styled">
                            <label for="checkbox_qu_04"><small>Military</small>
                            </label>
                        </li>
                        <li class="checkbox checkbox-primary">
                            <input id="checkbox_qu_05" type="checkbox" class="styled">
                            <label for="checkbox_qu_05"><small>Healthcare</small>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 text-right">
                    <a href="#" class="readmore">View All</a>
                </div>
            </div><!-- end row -->
        </form>
    </div><!-- end container -->
</div><!-- end section -->