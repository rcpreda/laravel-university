<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body">
                <div class="widget clearfix">
                    <div class="post-padding item-price">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="content-title">
                                    <h4><i class="fa fa-lock"></i> Login Account</h4>
                                </div><!-- end widget-title -->

                                <form id="submit" class="submit-form">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Username or Email">
                                            <input type="password" class="form-control" placeholder="*******">
                                            <button class="btn btn-primary">Login</button>
                                        </div>
                                    </div><!-- end row -->
                                </form>
                            </div><!-- end col -->

                            <div class="col-md-6">
                                <div class="content-title">
                                    <h4>No have account?</h4>
                                </div><!-- end widget-title -->

                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the</p>
                                <a href="#" class="btn btn-custom">Register Account</a>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end newsletter -->
                </div><!-- end post-padding -->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->