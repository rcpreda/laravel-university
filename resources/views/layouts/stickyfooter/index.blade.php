<div id="sitefooter-wrap" class="stickyfooter">
    <div id="sitefooter" class="container">
        <div id="copyright" class="row">
            <div class="col-md-6 col-sm-12 text-left">
                <p>Research Thesis ® is a registered trademark of <a href="#">University</a> INC.</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <ul class="list-inline text-right">
                    <li><a href="#">Terms of Usage</a></li>
                    <li><a href="#">Copyrights</a></li>
                    <li><a href="#">License</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>