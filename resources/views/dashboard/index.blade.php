@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection

@section('content')
    <div class="row">
        @include('layouts.sidemenu.left')



            <div class="content col-md-8" style="padding-top: 50px;">
                <div class="section-title text-center clearfix">
                    <h4>Main Dashboard</h4>
                    <hr>
                    <p class="lead">Lorem ipsum dolor sit amet, non odio tincidunt ut ante, lorem a euismod suspendisse vel, sed quam nulla mauris iaculis. Erat eget vitae malesuada, tortor tincidunt porta lorem lectus.</p>
                </div>

            <div class="post-padding" style="padding-top: 30px;">
            </div>
        </div>
    </div>
@endsection