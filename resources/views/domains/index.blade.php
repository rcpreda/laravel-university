@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Domains Management</h2>
            </div>
            <div class="pull-right">
                @can('role-create')
                    <a class="btn btn-success" href="{{ route('domains.create') }}"> Create New Permission</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Slug</th>
            <th>Approved</th>
            <th width="280px">Action</th>
        </tr>

        @if(!$domains->isEmpty())
            @foreach ($domains as $key => $val)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $val->name }}</td>
                    <td>{{ $val->slug }}</td>
                    <td>{{ $val->approved }}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('domains.show',$val->id) }}">Show</a>
                        @can('role-edit')
                            <a class="btn btn-primary" href="{{ route('domains.edit',$val->id) }}">Edit</a>
                        @endcan
                        @can('role-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $val->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No data</td>
            </tr>
        @endif



    </table>


    {!! $domains->render() !!}


@endsection