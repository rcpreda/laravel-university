@extends('layouts.main')

@section('background', 'lb')

@section('homehero')
    @include('layouts.homehero.book')
@endsection

@section('content')
    <div class="content col-md-12" style="padding-top: 50px;">
        <div class="post-padding" style="padding-top: 30px;">
            <div class="job-title nocover hidden-sm hidden-xs"><h3>Create Domain</h3></div>


            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('domains.index') }}">Back</a>
                    </div>
                </div>
            </div>


            @include('layouts.partials.errors.forms')

            {!! Form::open(['route' => 'domains.store','method'=>'POST', 'class' => 'submit-form']) !!}

            {!! Form::token(); !!}
            <div class="row">

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' =>'Domain name', 'required']) !!}
                        </div>
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-6 col-sm-12">

                        <div class="form-group">
                            {!! Form::label('approved', 'Status', ['class' => 'control-label']) !!}
                            {!! Form::select(
                                'approved',
                                App\Domain::$approval,
                                old('approved'),
                                ['class' => 'selectpicker', 'data-style' => 'btn-default', 'data-live-search' => 'false', 'placeholder' => 'Select...'])
                            !!}
                        </div>
                    </div>
                </div>

                <hr class="invis">

                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        {!! Form::label('description', 'Short Description', ['class' => 'control-label']) !!}
                        {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' =>'Domain short description']) !!}

                    </div>
                </div><!-- end row -->


                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Create Domain</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection