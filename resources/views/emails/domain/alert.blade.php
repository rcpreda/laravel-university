@component('mail::message')

@component('mail::panel')

    User {{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }}),

    with to publish a thesis in {{ $domain->name }} . Please login and approve/modify the new created domain.

    Thank you.

@endcomponent

@component('mail::button', ['url' => route('domains.index'), 'color' => 'success'])
    Manage Domains
@endcomponent

@endcomponent